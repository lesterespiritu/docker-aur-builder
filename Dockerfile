FROM archlinux/archlinux:base-devel

RUN printf "\nbuilder ALL=(ALL) NOPASSWD: /usr/bin/pacman" >> /etc/sudoers && \
    printf "\n[multilib]\nInclude = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf && \
    useradd -m builder && \
    printf "[color]\n\tui = auto\n[user]\n\tname = Builder\n\temail = builder@builder.docker.local" > /home/builder/.gitconfig && \
    chown builder:builder /home/builder/.gitconfig

VOLUME ["/builder", "/var/cache/pacman/pkg"]

USER builder
WORKDIR /builder
