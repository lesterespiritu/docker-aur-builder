#!/usr/bin/env bash

image="aur-builder"
workdir="$HOME/AUR"

hostname="https://aur.archlinux.org"
pkgname=""
pkgurl=""

buildargs="--syncdeps --clean"

noconfirm=0
force=0

main()
{
  parse_args "$@"

  printf "Building package $pkgname.\n"
  build_pkg
}

display_help()
{
  printf "\nbuild.sh\n"
  printf "Copyright (c) Lester Espiritu\n\n"
  printf "Usage:\n"
  printf "  ./build.sh [options]\n\n"
  printf "Options:\n"
  printf "  -u, --url <url>\t\tSet a custom repository. Implies package-name.\n"
  printf "  -p, --package-name <name>\tSet AUR package name.\n"
  printf "  -d, --directory <directory>\tSet builder working directory. Defaults to <home-dir>/AUR.\n"
  printf "  -n, --noconfirm\t\tNo confirmation.\n"
  printf "  -f, --force\t\t\tForce re-build of package.\n"
  printf "  -h, --help\t\t\tShow this help message.\n\n"
}

assert()
{
  [ "$?" -eq 0 ] || { printf "$1\n" ; exit 1 ; }
}

parse_args()
{
  [ "$#" -ne "0" ]
  assert "No arguments specified."

  while [ "$#" -gt 0 ]
  do
    arg="$1"
    case "$arg" in
      "-n" | "--noconfirm")
        noconfirm=1

        shift
      ;;
      "-f" | "--force")
        force=1

        shift
      ;;
      "-u" | "--url")
        shift

        [ "$#" -gt 0 ]
        assert "No value set for argument -u | --url."

        pkgurl="$1"
        pkgname="$(basename ${pkgurl%.*})"

        shift
      ;;
      "-d" | "--directory")
        shift

        [ "$#" -gt 0 ]
        assert "No value set for argument -d | --directory."

        workdir="$1"
        shift
      ;;
      "-p" | "--package-name")
        shift

        [ "$#" -gt 0 ]
        assert "No value set for argument -p | --package-name."

        [ ! -z "$pkgname" ] || pkgname="$1"
        shift
      ;;
      "-h" | "--help")
        display_help ; exit
      ;;
      *)
        printf "Unknown argument: $arg.\n"; exit
      ;;
    esac
  done

  [ "$noconfirm" -eq "1" ] && buildargs="$buildargs --noconfirm"
  [ "$force" -eq "1" ] && buildargs="$buildargs -f"

  [ ! -z "$pkgurl" ] || pkgurl="$hostname/${pkgname}.git"

  git ls-remote --quiet --exit-code "$pkgurl" &> /dev/null
  assert "Repository does not exist."
}

build_pkg()
{
  reposdir="$workdir/repos"
  depcachedir="$workdir/depcache"

  mkdir -p "$workdir"
  mkdir -p "$depcachedir"

  mkdir -p "$reposdir"

  pkgdir="$reposdir/$pkgname"

  [ -d "$pkgdir" ] && git -C "$pkgdir" pull --ff-only || git clone "$pkgurl" "$pkgdir"
  
  sudo -- bash -c " \
    docker build -t $image . && \
    docker run -it --rm -v $reposdir:/builder:rw -v $depcachedir:/var/cache/pacman/pkg:rw $image bash -c ' \
      sudo pacman -Syu && \
      cd $pkgname && \
      makepkg $buildargs'"
}

main "$@"; exit
