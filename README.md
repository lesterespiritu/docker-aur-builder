# Docker AUR Builder

A simple AUR package build script using a docker container.

## Usage

```
Usage:
  ./build.sh [options]

Options:
  -u, --url <url>               Set a custom repository. Implies package-name.
  -p, --package-name <name>     Set AUR package name.
  -d, --directory <directory>   Set builder working directory.
  -n, --noconfirm               No confirmation.
  -f, --force                   Force re-build of package.
  -h, --help                    Show this help message.
```
